import 'package:flutter/material.dart';

const Color colorPrimary = Color.fromRGBO(52, 152, 219, 1); // 메인 파란색
const Color colorDark = Color.fromRGBO(64, 64, 64, 1); // 검정
const Color colorDarkGrey = Color.fromRGBO(158, 158, 158, 1); // 진한 회색
const Color colorGrey = Color.fromRGBO(190, 190, 190, 1); // 회색
const Color colorLightGrey = Color.fromRGBO(243, 243, 243, 1); // 연회색
const Color colorInsideFont = Colors.white; // 메인색 안쪽 글자색
