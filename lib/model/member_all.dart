class MemberAll {
  num id;
  num userId;
  String username;
  DateTime joinDate;
  String memberGroupType;

  MemberAll(this.id, this.userId, this.username, this.joinDate, this.memberGroupType);

  factory MemberAll.fromJson(Map<String, dynamic> json) {
    return MemberAll(
        json['id'],
        json['userId'],
        json['username'],
        json['joinDate'],
        json['memberGroupType']
    );
  }
}