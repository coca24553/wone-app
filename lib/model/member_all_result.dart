import 'package:wone_app/model/member_all.dart';

class MemberAllResult {
  String msg;
  num code;
  List<MemberAll> list;
  num totalCount;

  MemberAllResult(this.msg, this.code, this.list, this.totalCount);

  factory MemberAllResult.fromJson(Map<String, dynamic> json) {
    return MemberAllResult(
        json['msg'],
        json['code'],
        json['List'] != null ? // null이 아니면 ?(삼항연산자)
          (json['list'] as List).map((e) => MemberAll.fromJson(e)).toList() : [],
        json['totalCount'],
        //람다식
        //json으로 string을 받아서 object(형태가 정해지지 않은 뭉텡이)의 list로 바꿈
        //list의 한 묶음을 e라고 정하고 이걸 던져줌
        // 다 작은그릇으로 바꿔치기 한다음에
        // 다시 그것들을 싹 가져와서 리스트로 촥 하고 정리해줌.
    );
  }
}