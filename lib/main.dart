import 'package:flutter/material.dart';
import 'package:wone_app/pages/add_new_card_screen.dart';
import 'package:wone_app/pages/calendar_page.dart';
import 'package:wone_app/pages/join_membership_page.dart';
import 'package:wone_app/pages/log_in_page.dart';
import 'package:wone_app/pages/main_page.dart';
import 'package:wone_app/pages/member_all_page.dart';
import 'package:wone_app/pages/member_page.dart';
import 'package:wone_app/pages/page_index.dart';
import 'package:wone_app/pages/setting_page.dart';
import 'package:wone_app/pages/spending_page.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.light(
          surfaceTint: Colors.white, // 배경색
          primary: Color(0xff3498db), // 헤더 백그라운드 색
          // onPrimary: Colors.black, // 헤더 텍스트 색
          onSurface: Colors.black // body 텍스트 색
        ),
      ),
      home: LogInPage(),
    );
  }
}