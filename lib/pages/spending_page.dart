import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';

class SpendingPage extends StatefulWidget {
  const SpendingPage({super.key});

  @override
  State<SpendingPage> createState() => _SpendingPage();
}

class _SpendingPage extends State<SpendingPage> {
  List<String> _items = []; // 리스트 선언
  String titleInput = "";
  String input = "";

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: Colors.white,
        shadowColor: Colors.black.withOpacity(appbarShadow),
        elevation: 2,
        title: Text(
          '지출 관리',
          style: TextStyle(
              fontFamily: 'NotoSans_Bold',
              fontSize: 20,
              letterSpacing: fontLetterSpacing,
              color: colorPrimary
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(
                      "지출 추가",
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeBig,
                        letterSpacing: fontLetterSpacing,
                        color: colorPrimary
                    ),
                  ),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.text, // 키보드 기본
                        maxLines: null, // 다중 라인 허용
                        onChanged: (String text) {
                          titleInput = text;
                        },
                        decoration: InputDecoration(
                          hintText: '지출 내용',
                          hintStyle: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            letterSpacing: fontLetterSpacing,
                            color: colorGrey,
                          )
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number, // 키보드 기본
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                        ],
                        maxLines: null, // 다중 라인 허용
                        onChanged: (String text) {
                          input = text;
                        },
                        decoration: InputDecoration(
                            hintText: '지출 금액',
                            hintStyle: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: fontLetterSpacing,
                              color: colorGrey,
                            )
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                      setState(() {
                        _items.add(titleInput);
                      });
                      Navigator.of(context).pop(); // 입력 후 창 닫힘
                    },
                        child: Text(
                          '취소',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeSm,
                            letterSpacing: fontLetterSpacing,
                          ),
                        )
                    ),
                    TextButton(onPressed: () {
                      setState(() {
                        _items.add(titleInput);
                      });
                      Navigator.of(context).pop(); // 입력 후 창 닫힘
                    },
                      child: Text(
                          '추가',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeSm,
                          letterSpacing: fontLetterSpacing,
                        ),
                      )
                    ),
                  ],
                );
              }
            );
          },
        label: Text('지출 추가'),
        icon: Icon(Icons.edit),
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView.builder(
      itemCount: _items.length,
        itemBuilder: (context, index) {
        return Dismissible(
            key: Key(_items[index]),
            child: Card(
              margin: EdgeInsets.all(10),
              color: colorLightGrey,
              shape: RoundedRectangleBorder(borderRadius:
              BorderRadius.circular(boxRadius),
            ),
          child: ListTile(
          title: Text(titleInput),
          isThreeLine: true,
          subtitle: Text(input),
          trailing: IconButton(icon: Icon(
                Icons.delete,
                color: Colors.blue,
              ),
              onPressed: () {
                setState(() {
                  _items.removeAt(index);
                });
              }),
            )
        ));
      });
  }
}
