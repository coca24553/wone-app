import 'package:flutter/material.dart';
import 'package:wone_app/component/component_member_all.dart';
import 'package:wone_app/repository/repo_member_all.dart';
import 'package:wone_app/model/member_all.dart';

class MemberAllPage extends StatefulWidget {
  const MemberAllPage({super.key});

  @override
  State<MemberAllPage> createState() => _MemberAllPageState();
}

SliverGridDelegate _sliverGridDelegate() {
  return const SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2,
    mainAxisExtent: 350,
    mainAxisSpacing: 1,
  );
}

State<MemberAllPage> createState() => _MemberAllPageState();
class _MemberAllPageState extends State<MemberAllPage> {
  List<MemberAll> _list = [];

  Future<void> _loadList() async {
    // 이 메서드가 repo 호출해서 데이터 받아온 다음에
    // setstate해서 _list 교체 할거임
    await RepoMemberAll().getMemberAlls()
        .then((res) => {
          print(res),
      setState(() {
        _list = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.builder(
        itemCount: _list.length,
        gridDelegate: _sliverGridDelegate(),
        itemBuilder: (BuildContext context, int idx){
          return ComponentMemberAll(
            memberAll: _list[idx],
            callback: () {},
          );
        },
      ),
    );
  }
}
