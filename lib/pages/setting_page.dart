import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/member_page.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "설정",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: fontSize22,
            letterSpacing: fontLetterSpacing,
            color: colorPrimary,
          ),
        ),
        leading: Icon(
          Icons.settings,
          color: colorPrimary,
          size: appbarIconSize,
        ),
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }
  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          // 상단 개인정보
          Container(
            margin: EdgeInsets.all(20),
            width: mediaQueryWidth,
            height: mediaQueryWidth * 0.18,
            child: TextButton(
                style: OutlinedButton.styleFrom(
                  backgroundColor: colorLightGrey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(boxRadius),
                    ),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MemberPage()));
                },
              child: Row(
                children: [
                  // 아이콘
                  Container(
                    height: mediaQueryWidth * 0.1,
                    width: mediaQueryWidth * 0.1,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: colorPrimary,
                    ),
                    child: Icon(
                      Icons.hourglass_bottom,
                      color: Colors.white,
                      size: mediaQueryWidth * 0.07,
                    ),
                  ),
                  // 회원 정보
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        // 홍길동
                        Container(
                          width: mediaQueryWidth / 2,
                          child: Row(
                            children: [
                              Container(
                                child: Text(
                                  "홍길동",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorPrimary,
                                  ),
                                ),
                              ),
                              Container(
                                child: Icon(
                                  Icons.chevron_right,
                                  color: colorPrimary,
                                ),
                              )
                            ],
                          ),
                        ),
                        // 15일째 도전중
                        Container(
                          width: mediaQueryWidth / 2,
                          child: Text(
                            "+15일째 도전 중",
                            style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeSm,
                              letterSpacing: fontLetterSpacing,
                              color: colorGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          // 문의
          Container(
            child: Column(
              children: [
                // 문의
                Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 0, 10),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                        child: Icon(
                          Icons.help_outline,
                          color: colorPrimary,
                          size: mediaQueryWidth * settingIconSize,
                        ),
                      ),
                      Container(
                        child: Text(
                          "문의",
                          style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSize22,
                            letterSpacing: fontLetterSpacing,
                            color: colorPrimary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // 이용방법
                Container(
                  margin: edgeInsetsSetting,
                  child: TextButton(
                      onPressed: () {},
                    child: Row(
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.5,
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.local_library_outlined,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "이용방법",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.35,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                // 공지사항
                Container(
                  margin: edgeInsetsSetting,
                  child: TextButton(
                      onPressed: () {},
                    child: Row(
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.5,
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.check_circle_outline,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "공지사항",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.35,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                // 고객센터
                Container(
                  margin: edgeInsetsSetting,
                  child: TextButton(
                      onPressed: () {},
                    child: Row(
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.5,
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.headset_outlined,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "고객센터",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.35,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
