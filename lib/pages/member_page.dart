import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/add_new_card_screen.dart';
import 'package:wone_app/pages/log_in_page.dart';

class MemberPage extends StatefulWidget {
  const MemberPage({
    super.key,
  });


  @override
  State<MemberPage> createState() => _MainPageState();
}

class _MainPageState extends State<MemberPage> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언
  String input = "";

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: Colors.white,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "개인정보",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: 22,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }

  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            // 아이콘
            Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 30),
              height: mediaQueryWidth * 0.25,
              width: mediaQueryWidth * 0.25,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: colorPrimary,
              ),
              child: Icon(
                Icons.person,
                color: Colors.white,
                size: mediaQueryWidth * 0.18,
              ),
            ),
            // 개인정보 내용
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              width: mediaQueryWidth,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Column(
                children: [
                  // 이름 입력 폼
                  Container(
                    height: mediaQueryWidth * 0.12,
                    child: Row(
                      children: [
                        // 이름
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '이름',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                color: colorDark,
                            ),
                          ),
                        ),
                        // 홍길동
                        Container(
                          width: mediaQueryWidth * 0.33,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        content: TextField(
                                          decoration: InputDecoration(hintText: '이름을 입력해주세요.'),
                                        ),
                                        actions: [
                                          TextButton(
                                            child: const Text('취소'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          TextButton(
                                            child: const Text('확인'),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    }
                                );
                              },
                              child: Text(
                                '홍길동',
                                style: TextStyle(
                                    letterSpacing: fontLetterSpacing,
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    fontSize: fontSizeBig,
                                  color: colorDarkGrey,
                                ),
                              ),
                          ),
                        ),
                        // 아이콘
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.05,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: mediaQueryWidth * 0.002,
                    color: colorDark,
                  ),

                  // 생년월일 입력 폼
                  Container(
                    height: mediaQueryWidth * 0.12,
                    child: Row(
                      children: [
                        // 생년월일
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '생년월일',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                              color: colorDark,
                            ),
                          ),
                        ),
                        // 날짜
                        Container(
                          width: mediaQueryWidth * 0.33,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                            onPressed: () async{
                              final selectedDate = await showDatePicker(
                                context: context, // 팝업으로 띄우기 때문에 context 전달
                                initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                                firstDate: DateTime(1980), // 시작 년도
                                lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                              );
                              if (selectedDate != null) {
                                setState(() {
                                  date = selectedDate; // 선택한 날짜는 dart 변수에 저장
                                });
                              }
                            },
                            style: TextButton.styleFrom(
                              padding: EdgeInsets.zero,
                            ),
                            child: Text(
                              '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
                              style: TextStyle(
                                fontSize: fontSizeMid,
                                  letterSpacing: fontLetterSpacing,
                                color: colorDarkGrey,
                              ),
                            ),
                          ),
                        ),
                        // 아이콘
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.05,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: mediaQueryWidth * 0.002,
                    color: colorDark,
                  ),

                  // 비밀번호 입력 폼
                  Container(
                    height: mediaQueryWidth * 0.12,
                    child: Row(
                      children: [
                        // 비밀번호
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '비밀번호',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                              color: colorDark,
                            ),
                          ),
                        ),
                        // ****
                        Container(
                          width: mediaQueryWidth * 0.33,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: TextField(
                                        decoration: InputDecoration(hintText: '비밀번호를 입력해주세요.'),
                                        obscureText: true, //비밀번호 안 보이게 함
                                      ),
                                      actions: [
                                        TextButton(
                                          child: const Text('취소'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: const Text('확인'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  }
                              );
                            },
                            child: Text(
                              '****',
                              style: TextStyle(
                                  letterSpacing: fontLetterSpacing,
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeBig,
                                color: colorDarkGrey,
                              ),
                            ),
                          ),
                        ),
                        // 아이콘
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.05,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: mediaQueryWidth * 0.002,
                    color: colorDark,
                  ),

                  // 카드 입력 폼
                  Container(
                    height: mediaQueryWidth * 0.12,
                    child: Row(
                      children: [
                        // 카드
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '카드',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                              color: colorDark,
                            ),
                          ),
                        ),
                        // 현재 카드
                        Container(
                          width:mediaQueryWidth * 0.33,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: Text(
                                        '현재 사용 중인 카드를 변경하시겠습니까?',
                                      ),
                                      actions: [
                                        TextButton(
                                          child: const Text('취소'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: const Text('확인'),
                                          onPressed: () {
                                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddNewCardScreen()));
                                          },
                                        ),
                                      ],
                                    );
                                  }
                              );
                            },
                            child: Text(
                              '농협',
                              style: TextStyle(
                                  letterSpacing: fontLetterSpacing,
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeBig,
                                color: colorDarkGrey,
                              ),
                            ),
                          ),
                        ),
                        // 아이콘
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.05,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // 로그아웃 하기
            Container(
              alignment: FractionalOffset.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 5),
              width: mediaQueryWidth,
              height: mediaQueryWidth * 0.12,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Container(
                width: mediaQueryWidth,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                              '정말 로그아웃 하시겠습니까?',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMid,
                                  letterSpacing: fontLetterSpacing,
                              ),
                            ),
                            actions: [
                              TextButton(
                                child: const Text('취소'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: const Text('확인'),
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => LogInPage()));
                                },
                              ),
                            ],
                          );
                        }
                    );
                  },
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  ),
                  child: Text(
                    '로그아웃 하기',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        fontSize: fontSizeSm,
                      color: colorDarkGrey,
                    ),
                  ),
                ),
              ),
            ),
            // 회원탈퇴 하기
            Container(
              alignment: FractionalOffset.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 5),
              width: mediaQueryWidth,
              height: mediaQueryWidth * 0.12,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Container(
                width: mediaQueryWidth,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                              '정말 회원탈퇴 하시겠습니까?',
                              style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMid,
                                letterSpacing: fontLetterSpacing,
                              ),
                            ),
                            actions: [
                              TextButton(
                                child: const Text('취소'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: const Text('확인'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        }
                    );
                  },
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  ),
                  child: Text(
                    '회원탈퇴 하기',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        fontSize: fontSizeSm,
                      color: colorDarkGrey,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}